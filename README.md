# Infinite Monkeys
Stock Market Prediction with Data Science Tools

## Objective
Break the market

## Real Objectives
- Get upto speed with latest Python data science tools
- Utilize OO framework
- Agile approach with Git, CI/CD, Jira
- Integrate both price and sentiment features for predictions
- Studying and implementing various ML algorithms 
- JS React for dashboard
- Open-source for community support
- Break the market


## Specifications
- python v3.8.8
- requirements.txt for venv

## ETL Workflow 
![ETL Flowchart](./Images/ETL_Capstone.png)

## Example Figures and Dashboard Screenshot
![Stock Analysis (filtered value graph)](./Images/stock_graph.png)
![Stock Analysis (seasonality)](./Images/stock_analysis_seasonality.png)
![Dashboard Screenshot)](./Images/dashboard_ss.png)
