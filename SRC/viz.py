import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input
import plotly.graph_objects as go
import plotly.express as px
from matplotlib import pyplot as plt

from etl import ETL
from asys import Asys
from model import Model
from init import INIT
import pandas as pd


class Viz:
    # init method or constructor
    def __init__(self, name):
        self.app_name = name
        self.app = []

    # Create Dashboard
    def create_dash(self, init_data, all_tickers):
        data = init_data.old
        default_stock = init_data.name

        external_stylesheets = [
            {
                "href": "https://fonts.googleapis.com/css2?"
                        "family=Lato:wght@400;700&display=swap",
                "rel": "stylesheet",
            },
        ]
        self.app = dash.Dash(__name__)
        self.app.title = "Market Predictor: Break the Market!"
        self.app.layout = \
            html.Div(
                children=[
                    # Header
                    html.Div(
                        children=[
                            html.P(children="📈", className="header-emoji"),
                            html.H1(children="Market Predictor", className="header-title",
                                    ),
                            html.P(
                                children="Analysis of market behavior"
                                         " in NYSE and Nasdaq and stock value prediction"
                                         " based on data-driven modeling",
                                className="header-description",
                            ),
                        ],
                        className="header",
                    ),
                    # Stock Selector, Dates, Plots
                    html.Div(
                        children=[
                            # Stock Selector
                            html.Div(
                                children=[
                                    html.Div(children="Ticker", className="menu-title"),
                                    dcc.Dropdown(
                                        id="ticker-filter",
                                        options=[
                                            {"label": ticker, "value": ticker}
                                            for ticker in all_tickers
                                        ],
                                        value=default_stock,
                                        clearable=False,
                                        className="dropdown",
                                    ),
                                ]
                            ),
                            # Calendar
                            html.Div(
                                children=[
                                    html.Div(children="Date Range", className="menu-title"),
                                    dcc.DatePickerRange(
                                        id="date-range",
                                        min_date_allowed=data.High.axes[0].min().date(),
                                        max_date_allowed=data.High.axes[0].max().date(),
                                        start_date=data.High.axes[0].min().date(),
                                        end_date=data.High.axes[0].max().date(),
                                    ),
                                ]
                            ),
                        ],
                        className="menu",
                    ),
                    # Plots
                    html.Div(
                        children=[
                            # Plots: Daily High Values
                            html.Div(
                                children=dcc.Graph(
                                    id="price-chart",
                                    config={"displayModeBar": False},
                                ),
                                className="card",
                            ),
                            # Plots: Daily Close Values
                            html.Div(
                                children=dcc.Graph(
                                    id="diagnostic-chart",
                                    config={"displayModeBar": False},
                                ),
                                className="card",
                            ),
                        ],
                        className="wrapper",
                    ),
                ]
            )

        @self.app.callback(
            [Output("price-chart", "figure"), Output("diagnostic-chart", "figure")],
            [
                Input("ticker-filter", "value"),
                Input("date-range", "start_date"),
                Input("date-range", "end_date"),
            ],
        )
        def update_charts(ticker, start_date, end_date):
            # Read parameters from the config file
            config_init = INIT('config.json')
            config_init.read_json()
            param_filter = config_init.config_file_data['filter_param']
            param_fig = config_init.config_file_data['fig_sizes']
            param_model = config_init.config_file_data['analysis_model_param']

            stock = ETL(ticker)
            stock.stock_data(start_date, end_date)
            data_update = stock.old

            stock_analysis = Asys()
            stock_analysis.filter(data_update.High, param_filter[0]["win_size"], param_filter[0]["poly_order"])
            stock_model = Model()

            # ARIMA timeseries model
            stock_model.model_arima(stock_analysis.filtered,
                                    param_model[0]["p_start"],
                                    param_model[0]["q_start"],
                                    param_model[0]["p_max"],
                                    param_model[0]["q_max"],
                                    param_model[0]["m"],
                                    param_model[0]["data_split_train_perc"],
                                    param_fig[0]["width_larger_plots"],
                                    param_fig[0]["height_larger_plots"])

            # Forecast future values
            stock_model.forecast_val(param_model[0]["alpha"],
                                     param_model[0]["dpi"],
                                     param_fig[0]["width"],
                                     param_fig[0]["height"],
                                     ticker,
                                     ' Daily High Value',
                                     False)

            train_data = pd.Series(stock_model.train_data, name="Value (USD)")
            train_data = train_data.to_frame()
            test_data = pd.Series(stock_model.test_data, name="test_vals")
            test_data = test_data.to_frame()
            fig = px.line(train_data, y="Value (USD)", labels={"Actual"}, title="Daily High Values")
            predicted_data = pd.concat(
                [stock_model.confidence_lolim, stock_model.confidence_hilim, stock_model.forecast_data],
                axis=1)
            fig.add_trace(go.Scatter(name="Actual", x=test_data.index, y=test_data.test_vals))
            fig.add_trace(go.Scatter(name="Predicted", x=predicted_data.index, y=predicted_data.forecast_vals))
            fig.add_trace(go.Scatter(name="95% Conf Lim High", x=predicted_data.index, y=predicted_data.conf_hilim,
                                     line=dict(color='cadetblue', width=4,
                                               dash='dash'))
                          )
            fig.add_trace(go.Scatter(name="95% Conf Lim Low", x=predicted_data.index, y=predicted_data.conf_lolim,
                                     line=dict(color='cadetblue', width=4,
                                               dash='dash'))
                          )

            # Plotly figure object
            price_chart_figure = fig

            fig_diag = plt.figure(figsize=(param_fig[0]["width_larger_plots"],
                                           param_fig[0]["height_larger_plots"]))
            stock_model.model_auto.plot_diagnostics(fig=fig_diag)
            plt.show(block=False)
            plt.close('all')
            diagnostic_chart_figure = stock_model.plotly_diagnostic(fig_diag)

            return price_chart_figure, diagnostic_chart_figure
