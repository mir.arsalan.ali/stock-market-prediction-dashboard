import yfinance as yf


class ETL:
    # init method or constructor
    def __init__(self, name):
        self.name = name
        self.ticker = yf.Ticker(name)
        self.info = self.ticker.info
        self.action = self.ticker.actions
        self.dividends = self.ticker.dividends
        self.splits = self.ticker.splits
        self.old = []

    def stock_data(self, date_start, date_end):
        self.old = self.ticker.history(start=date_start, end=date_end)
