import matplotlib.pyplot as plt
from etl import ETL
from asys import Asys
from init import INIT
from model import Model
from viz import Viz
import warnings
warnings.filterwarnings('ignore')


def main():
    # --- INITIALIZE ---
    config_init = INIT('config.json')

    # Read parameters from the config file
    config_init.read_json()
    param_stock = config_init.config_file_data['stock_details']
    param_filter = config_init.config_file_data['filter_param']
    param_fig = config_init.config_file_data['fig_sizes']
    param_model = config_init.config_file_data['analysis_model_param']
    stock_master_list = config_init.config_file_data['ticker_master_list']

    # --- ETL ---
    # Pull stock info + data around provided dates
    curr_stock = ETL(param_stock[0]["stock_name"])
    curr_stock.stock_data(param_stock[0]["start_date"], param_stock[0]["end_date"])

    # --- ANALYSIS ---
    stock_analysis = Asys()

    # Plot SavGol filtered and raw values
    stock_analysis.filter(curr_stock.old.High,
                          param_filter[0]["win_size"],
                          param_filter[0]["poly_order"])

    # [Optional Plot] filtered values
    # stock_analysis.plot_raw_filtered(curr_stock.old.High,param_fig[0]["width"],
    #                                  param_fig[0]["height"], curr_stock.name, ' Daily High Value')

    # [Optional Plot] Moving mean and STD
    # Check for stationarity of timeseries
    # stock_analysis.check_stationarity(curr_stock.old.High, param_model[0]["moving_win_size"],
    #                                   param_fig[0]["width"], param_fig[0]["height"],
    #                                   param_fig[0]["width_larger_plots"], param_fig[0]["height_larger_plots"],
    #                                   curr_stock.name, ' Daily High Value')

    # --- MODELING ---
    stock_model = Model()

    # ARIMA timeseries model
    stock_model.model_arima(stock_analysis.filtered,
                            param_model[0]["p_start"],
                            param_model[0]["q_start"],
                            param_model[0]["p_max"],
                            param_model[0]["q_max"],
                            param_model[0]["m"],
                            param_model[0]["data_split_train_perc"],
                            param_fig[0]["width_larger_plots"],
                            param_fig[0]["height_larger_plots"])

    # Diagnostics: Q-Q plot; Correlogram; etc.
    fig_diag = plt.figure(figsize=(param_fig[0]["width_larger_plots"],
                                   param_fig[0]["height_larger_plots"]))
    stock_model.model_auto.plot_diagnostics(fig=fig_diag)
    plt.show(block=False)
    plt.close('all')
    diagnostic_figure = stock_model.plotly_diagnostic(fig_diag)

    # Forecast future values
    stock_model.forecast_val(param_model[0]["alpha"],
                             param_model[0]["dpi"],
                             param_fig[0]["width"],
                             param_fig[0]["height"],
                             curr_stock.name,
                             ' Daily High Value',
                             False)

    # --- VISUALIZATION ---
    stock_dash = Viz('market_pred_dash')
    stock_dash.create_dash(curr_stock, set(stock_master_list[0]['all_tickers']))
    stock_dash.app.run_server(debug=True)


if __name__ == '__main__':
    main()

    # # Additional Note:
    # # To get any of the key info, e.g., debt-to-equity, use key, e.g., all_data[0].info.get('debtToEquity')
